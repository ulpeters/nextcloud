# nextcloud in docker

## Prerequisites
It is assumed that on the docker host we're already running
1. a [jwilder/nginx-proxy](https://github.com/jwilder/nginx-proxy) reverse proxy
2. together with [nginx-proxy/acme-companion](https://github.com/nginx-proxy/acme-companion) to obtain letsencrypt TLS certificates

## Used volumes
- `./data/webroot` - all files and the sqlite database

## Installation & Configuration

1. Clone the repo this repo to /opt/docker
2. Copy `.env.template` to `.env` and setup your hostname, admin user and password
3. Run `docker-compose up` and watch out for possible errors. If everything goes fine, re-run with `-d`.